package id.findmeetingplaces;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Point houseA = new Point(5, 5);
        Point houseB = new Point(5, 4);
        Point houseC = new Point(5, 3);
        Point houseD = new Point(4, 5);
        Point houseE = new Point(4, 4);
        Point houseF = new Point(4, 3);
        Point houseG = new Point(3, 5);
        Point houseH = new Point(3, 4);
        Point houseI = new Point(3, 3);
        Point houseJ = new Point(2, 2);
        Point houseLocations[] = {houseA, houseB, houseC, houseD, houseE, houseF, houseG, houseH, houseI, houseJ};
        List<Point> meetingPlaces = findMeetingPlaces(houseLocations, houseJ, 2);
        try {
            for (Point meetingPlace : meetingPlaces)
                System.out.println("Possible meeting place : " + meetingPlace.x + ", " + meetingPlace.y);
        } catch (Exception e) {
            System.out.println("No meeting place possible.");
        }
    }

    private static List<Point> findMeetingPlaces(Point[] houseLocations, Point grandPaHouse, int D) {
        List<Point> meetingPlace = new ArrayList<>();
        int distance = D / 2;
        for (Point place : houseLocations)
            if (Math.abs(grandPaHouse.x - place.x) <= distance && Math.abs(grandPaHouse.y - place.y) <= distance)
                meetingPlace.add(place);
        return meetingPlace;
    }
}
